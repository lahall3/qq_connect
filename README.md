QQ connect plugin for Discourse / Discourse QQ 互联插件
Authenticate with discourse with qq connect.

通过 QQ 互联登陆 Discourse。

Register Client Key & Secert / 申请 QQ 接入
登录 QQ Connect，注册填写相关信息。
进入管理中心，点击创建应用，选择网站。
填写相关信息。网站地址应填写论坛所处的位置。回调地址应填写根域名位置。如图所示。（验证所需要的标签可在 Discourse 设置中插入，验证后即可删除；访问 Discourse 管理面板 - 内容 - 页面顶部）
找到刚申请到的应用，在左上角找到id和key，分别填入 Discourse 设置中的 client_key 和 client_secret。


Installation / 安装
在 app.yml 的

hooks:
  after_code:
    - exec:
        cd: $home/plugins
        cmd:
          - mkdir -p plugins
          - git clone https://github.com/discourse/docker_manager.git
最后一行 - git clone https://github.com/discourse/docker_manager.git 后添加：

- git clone https://github.com/fantasticfears/qq_connect.git
Usage / 使用
Go to Site Settings's login category, fill in the client id and client secret.

进入站点设置的登录分类，填写 client id 和 client serect。

Issues / 问题
Visit topic on Discourse Meta or GitHub Issues.

访问中文论坛上的主题或GitHub Issues。

Changelog
Current version: 0.4.0

0.3.0: 修正没有正确保存 uid 的 bug。 0.4.0: 包含登录策略 gem，去掉下载外部 gem 的步骤。







